
<?php 
//Algorihmic
  function sumOfDigitsFrom1ToN($n) 
  { 
      $result = 0; // initialize result 
    
      for ($x = 1; $x <= $n; $x++) 
          $result += sumOfDigits($x); 
    
      return $result; 
  } 
    
  
  function sumOfDigits($x) 
  { 
      $sum = 0; 
      while ($x != 0) 
      { 
          $sum += $x %10; 
          $x = $x /10; 
      } 
      return $sum; 
  } 
        
      $n = 4; 
      echo "Sum of digits in numbers from" 
                 . " 1 to " . $n . " is " 
                 . sumOfDigitsFrom1ToN($n); 
        
  ?> 