DECLARE @StartDate DATE = '1/1/2011', 
        @EndDate   DATE = '30/6/2013' 

SELECT * 
FROM   assessments 
WHERE  Datediff(date, created_at, @StartDate) <= 0 
       AND Datediff(date, created_at, @EndDate) >= 0 