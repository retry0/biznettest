<?php 
//primer
function isPrimer($n) 
{ 
	// Corner case 
	if ($n <= 1) 
		return false; 

	// Check from 2 to n-1 
	for ($i = 2; $i < $n; $i++) 
		if ($n % $i == 0) 
			return false; 

	return true; 
} 

// Function to print primes 
function printPrimer($n) 
{ 
	for ($i = 2; $i <= $n; $i++) 
	{ 
		if (isPrimer($i)) 
			echo $i . " "; 
	} 
} 

// array
$n = 100; 
printPrimer($n); 

?> 
